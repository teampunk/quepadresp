# -*- coding: utf-8 -*-
import os
from django import template
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import get_template
from django.template import Context
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from .models import *
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from django.core.urlresolvers import reverse
from django.utils.html import strip_tags
from django.db.models import Count
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import timezone
import json
from json import dumps
from django.core import serializers  
from .forms import *
from tagging.models import Tag, TaggedItem
import django.shortcuts as shortcuts
from django.shortcuts import render, get_object_or_404


class HomeView(ListView):
    model = New
    template_name = 'home.html'
    paginate_by = 10
    context_object_name = 'news'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['section'] = Section.objects.get(slug='home')
        p = Paginator(New.objects.all().order_by('date'), self.paginate_by)
        most_viewed = New.objects.annotate(Count('titulo')).order_by('-titulo')[:5]
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        context['section_fetiche'] = FeticheItem.objects.all().order_by('?').distinct()[:3]
        popup = "True"
        
        context['news'] = p.page(context['page_obj'].number)
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        context['categorias'] = CategoryNew.objects.all()
        context['popup'] = popup

        return context

class CategoryView(ListView):
    model = New
    context_object_name = 'noticias'
    template_name = 'category.html'
    paginate_by = 10

    def get_queryset(self):
        return New.objects.filter(categoria__slug=self.kwargs['slug']).order_by('date')

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['categorias'] = CategoryNew.objects.all()
        most_viewed = New.objects.annotate(Count('titulo'))[:5]
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        context['slug'] = self.kwargs['slug']
        context['section'] = CategoryNew.objects.get(slug=self.kwargs['slug'])
        current_category = CategoryNew.objects.get(slug=self.kwargs['slug'])
        fs_id = current_category.section_fetiche.id
        context['section_fetiche'] = FeticheItem.objects.filter(section_fetiche__pk=fs_id).order_by('?').distinct()[:3]
        popup = "True"
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        context['popup'] = popup
        #context['now'] = timezone.now()
        return context

class NewsDetailView(DetailView):
    model = New
    context_object_name = 'new'
    # Sacar los parametros de base de datos o sesion.
    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        context['categorias'] = CategoryNew.objects.all()
        most_viewed = New.objects.annotate(Count('titulo'))[:5]
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        related_news = New.objects.all().exclude(slug=self.kwargs['slug']).order_by('?')[:3]
        context['banners'] = BannerItem.objects.filter(banner_in_section__pk=5)
        context['promotion_banner'] = PromotionBanner.objects.get(pk=3)
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        context['related_news'] = related_news
        popup = "True"
        context['popup'] = popup
        # print categories_selected
        return context

    def get_template_names(self):
        return 'news_detail.html'

class VideosView(ListView):
    model = Video
    context_object_name = 'videos'
    template_name = 'videos.html'
    paginate_by = 10

    def get_queryset(self):
        return Video.objects.all()
    # Sacar los parametros de base de datos o sesion.
    def get_context_data(self, **kwargs):
        context = super(VideosView, self).get_context_data(**kwargs)
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        context['section'] = Section.objects.get(slug='videos')
        context['categorias'] = CategoryNew.objects.all()
        most_viewed = New.objects.annotate(Count('titulo'))[:5]
        current_category = Section.objects.get(slug='videos')
        fs_id = current_category.section_fetiche.id
        context['section_fetiche'] = FeticheItem.objects.filter(section_fetiche__pk=fs_id).order_by('?').distinct()[:3]
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        popup = "True"
        context['popup'] = popup
        # print categories_selected
        return context

    # def get_template_names(self):
    #     return 'videos.html'

def contactUs(request):
    categorias = CategoryNew.objects.all()
    slug = 'suscripcion'
    section = Section.objects.get(slug=slug)
    error = ''
    form = None
    if request.method == 'POST':
        form = FormContacto(request.POST)
        # form = Contacto(request.POST)
        if form.is_valid():


            from_mail = 'info@quepadreserpadres.com'
            user_name = form.cleaned_data['user_name']
            user_sex = form.cleaned_data['user_sex']
            user_age = form.cleaned_data['user_age']
            user_city = form.cleaned_data['user_city']
            user_state = form.cleaned_data['user_state']
            user_email = form.cleaned_data['user_email']

            subject = 'Nueva solicitud de suscripcion a NEWSLETTER - quepadreserpadres.con/suscripcion'
            email_body = subject + '\n'
            email_body += '\nNombre: ' + user_name
            email_body += '\nCorreo: ' + user_email
            email_body += '\nSexo: ' + user_sex
            email_body += '\nEdad: ' + user_age
            email_body += '\nCiudad: ' + user_city
            email_body += '\nEstado: ' + user_state

            recipients = ['info@quepadreserpadres.com']
            #recipients = ['sebas@punkmkt.com']
            mail_data = EmailMessage(subject, email_body, from_mail, recipients)
            mail_data.encoding = "utf-8"
            mail_data.send()

            contacto = Contacto(nombre_contacto=user_name,sexo=user_sex,edad=user_age,ciudad=user_city,estado=user_state,empresa='',correo_electronico=user_email)
            contacto.save()
            return HttpResponseRedirect(reverse('registrok'))

        else:
            error = 'faild'
    
    return render(request, 'contact-us.html', {'categorias':categorias, 'slug':slug, 'section':section, 'error':error, 'form':form})


def franquiciatario(request):
    categorias = CategoryNew.objects.all()
    section = Section.objects.get(slug='franquiciatario')

    error = ''
    form = None
    if request.method == 'POST':
        form = FormFranquiciatario(request.POST)
        if form.is_valid():
            from_mail = 'info@quepadreserpadres.com'
            user_name = form.cleaned_data['user_name']
            user_email = form.cleaned_data['user_email']
            user_phone_number = form.cleaned_data['user_phone_number']
            user_message = form.cleaned_data['user_message']

            subject = 'Nueva solicitud de suscripcion de franquiciatario - quepadreserpadres.con/franquiciatario'
            email_body = subject + '\n'
            email_body += '\nNombre: ' + user_name
            email_body += '\nCorreo: ' + user_email
            email_body += '\nNo. de Tel.: ' + str(user_phone_number)
            email_body += '\nMensaje: ' + user_message

            recipients = ['info@quepadreserpadres.com']
            #recipients = ['sebas@punkmkt.com']
            mail_data = EmailMessage(subject, email_body, from_mail, recipients)
            mail_data.encoding = "utf-8"
            mail_data.send()

            franquicitario = Franquiciatario(nombre_contacto=user_name,correo_electronico=user_email,telefono=user_phone_number,comentario=user_message)
            franquicitario.save()

            return HttpResponseRedirect(reverse('datos_enviados'))
        else:
            error = 'faild'

    return render(request, 'franquiciatario.html', {'categorias':categorias, 'section':section, 'error':error, 'form':form})

def terms_and_conditions(request):
    categorias = CategoryNew.objects.all()
    list_most_viewed = New.objects.annotate(Count('titulo'))[:5]
    latest_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
    banners = BannerItem.objects.filter(banner_in_section__pk=5)
    popup = "True"
    return render(request, 'terminos-y-condiciones.html', {'categorias':categorias, 'list_most_viewed':list_most_viewed, 'latest_videos':latest_videos, 'popup':popup, 'banners':banners})

def search_news(request):
    if request.is_ajax():
        phrase = request.GET['phrase']
        news = New.objects.filter(titulo__icontains=phrase)
        results = []
        for new in news:
            new_json = {}
            new_json['name'] = new.titulo
            new_json['slug'] = "/"+new.categoria.slug +"/"+ new.slug
            results.append(new_json)
        data = dumps(results)
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def tagged_posts(request, tag_name):
    query_tag = get_object_or_404(Tag, name=tag_name)
    categorias = CategoryNew.objects.all()
    list_most_viewed = New.objects.annotate(Count('titulo'))[:5]
    latest_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
    banners = BannerItem.objects.filter(banner_in_section__pk=5)
    promotion_banner = PromotionBanner.objects.get(pk=3)

    popup = "True"
    entries = TaggedItem.objects.get_by_model(New, query_tag)
    
    return render(request, 'resultado-de-busqueda.html', {'entries':entries, 'categorias':categorias, 'list_most_viewed':list_most_viewed, 'latest_videos':latest_videos, 'popup':popup, 'banners':banners, 'promotion_banner':promotion_banner})

def datos_enviados(request):
    categorias = CategoryNew.objects.all()
    return render(request, 'datos-enviados.html', {'categorias':categorias})

def registrok(request):
    categorias = CategoryNew.objects.all()
    return render(request, 'suscripcion-exitosa.html', {'categorias':categorias})

def landing(request):
    return render(request, 'landing.html', {})        