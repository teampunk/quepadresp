from django import forms

class FormBuscarElementos(forms.Form):
	term = forms.CharField(max_length=100)

class FormContacto(forms.Form):
    user_name = forms.CharField(max_length=100, required=True)
    user_sex = forms.CharField(max_length=13, required=True)
    user_age = forms.CharField(max_length=80, required=False)
    user_city = forms.CharField(max_length=80, required=True)
    user_state = forms.CharField(max_length=80, required=False)
    user_email = forms.EmailField(required=True)

class FormFranquiciatario(forms.Form):
    user_name = forms.CharField(max_length=100, required=True)
    user_email = forms.EmailField(required=True)
    user_phone_number = forms.CharField(required=True, max_length=20)
    user_message = forms.CharField(widget=forms.Textarea, max_length=250,required=False)