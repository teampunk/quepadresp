from django.contrib import sitemaps
from django.core.urlresolvers import reverse
from main.models import *

class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['home', 'videos_view', 'suscripcion', 'franquiciatario', 'terms_and_conditions']

    def location(self, item):
        return reverse(item)

class CategoryViewSiteMap(sitemaps.Sitemap):
	priority = 0.5
	changefreq = 'daily'

	def items(self):
		return CategoryNew.objects.all()

class NewViewSiteMap(sitemaps.Sitemap):
	priority = 0.5
	changefreq = 'daily'

	def items(self):
		return New.objects.all()