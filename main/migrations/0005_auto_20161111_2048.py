# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_remove_new_imagen_slide'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contacto',
            name='ciudad',
            field=models.CharField(max_length=100, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='contacto',
            name='edad',
            field=models.CharField(max_length=10, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='contacto',
            name='empresa',
            field=models.CharField(max_length=100, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='contacto',
            name='estado',
            field=models.CharField(max_length=100, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='contacto',
            name='sexo',
            field=models.CharField(max_length=100, blank=True, null=True),
        ),
    ]
