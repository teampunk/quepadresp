# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_new_tags'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tagitem',
            name='tag',
        ),
        migrations.RemoveField(
            model_name='new',
            name='etiquetas',
        ),
        migrations.DeleteModel(
            name='Tag',
        ),
        migrations.DeleteModel(
            name='TagItem',
        ),
    ]
