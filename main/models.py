from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from ckeditor.fields import RichTextField
from django.utils.text import slugify
from django.template.defaultfilters import slugify
from autoslug import AutoSlugField
from functools import reduce
from tagging.fields import TagField
from tagging.models import Tag


# Create your models here.
def get_model_attr(instance, attr):
    """Example usage: get_model_attr(instance, 'category__slug')"""
    for field in attr.split('__'):
        instance = getattr(instance, field)
    return instance

class Test(models.Model):
    date = models.DateField()
    title = models.TextField()

class Author(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.name

class Book(models.Model):
    name = models.CharField('Book name', max_length=100)
    author = models.ForeignKey(Author, blank=True, null=True)
    author_email = models.EmailField('Author email', max_length=75, blank=True)
    imported = models.BooleanField(default=False)
    published = models.DateField('Published', blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    categories = models.ManyToManyField(Category, blank=True)

    def __unicode__(self):
        return self.name

class PromotionBanner(models.Model):
    titulo = models.CharField(max_length=100, blank=True, null=True)
    banner_url = models.CharField(max_length=1000, blank=True)
    banner_image = models.ImageField(upload_to='Images/PromotionBanners/')
    def __unicode__(self):
        return self.titulo

class BannerInSection(models.Model):
    titulo = models.CharField(max_length=100, blank=True, null=True)
    def __unicode__(self):
        return self.titulo

class BannerItem(models.Model):
    titulo = models.CharField(max_length=50,blank=True,null=True)
    descripcion = models.CharField(max_length=250, blank=True)
    slide_url = models.CharField(max_length=500, blank=True)
    image = models.ImageField(upload_to='Images/Benner/Items/')
    banner_in_section = models.ForeignKey(BannerInSection, blank=True, null=True)
    def __unicode__(self):
        return self.titulo

class SectionSlide(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)
    def __unicode__(self):
        return self.nombre

class SectionSlideItem(models.Model):
    titulo = models.CharField(max_length=50,blank=True,null=True)
    descripcion = models.CharField(max_length=250, blank=True)
    slide_url = models.CharField(max_length=500, blank=True)
    image = models.ImageField(upload_to='Images/Section/Slide/')
    section_slide = models.ForeignKey(SectionSlide, blank=True, null=True)
    def __unicode__(self):
        return self.titulo

class SectionFetiche(models.Model):
    titulo = models.CharField(max_length=50,blank=True, null=True)
    def __unicode__(self):
        return self.titulo

class FeticheItem(models.Model):
    titulo = models.CharField(max_length=50,blank=True,null=True)
    image = models.ImageField(upload_to='Images/Section/Fetiche/')
    section_fetiche = models.ForeignKey(SectionFetiche, blank=True, null=True)
    def __unicode__(self):
        return self.titulo

# class Tag(models.Model):
#     name = models.CharField('Tags name', max_length=100,blank=True, null=True)

#     def __unicode__(self):
#         return self.name

# class TagItem(models.Model):
#     tag_texto = models.CharField('Tag', max_length=30, blank=True, null=True)
#     tag = models.ForeignKey(Tag, blank=True, null=True)
#     def __unicode__(self):
#         return self.tag_texto

class Metatag(models.Model):
    titulo = models.CharField(max_length=100,blank=True, null=True)
    descripcion = models.CharField(max_length=1000,blank=True)
    keywords = models.CharField(max_length=1000,blank=True)
    def __unicode__(self):
        return self.titulo

class Video(models.Model):
    titulo = models.CharField(max_length=200, blank=True, null=True)
    descripcion = RichTextField(max_length=10000, blank=True, null=True)
    id_video = models.CharField(max_length=20, blank=True)

class Section(models.Model):
    titulo = models.CharField('Nombre de Seccion', max_length=100, null=True)
    descripcion = RichTextField(max_length=10000, blank=True, null=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    section_slide = models.ForeignKey(SectionSlide, blank=True, null=True)
    banner = models.ForeignKey(BannerInSection, blank=True, null=True)
    promotion_banner = models.ForeignKey(PromotionBanner, blank=True, null=True)
    metatag = models.ForeignKey(Metatag, blank=True, null=True)
    section_fetiche = models.ForeignKey(SectionFetiche, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(Section, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.titulo

class CategoryNew(models.Model):
    name = models.CharField('Nombre de Categoria', max_length=100, blank=True, null=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    category_slide = models.ForeignKey(SectionSlide, blank=True, null=True)
    banner = models.ForeignKey(BannerInSection, blank=True, null=True)
    promotion_banner = models.ForeignKey(PromotionBanner, blank=True, null=True)
    metatag = models.ForeignKey(Metatag, blank=True, null=True)
    section_fetiche = models.ForeignKey(SectionFetiche, blank=True, null=True)
    promotion_banner = models.ForeignKey(PromotionBanner, blank=True, null=True)
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(CategoryNew, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('category_view', kwargs={'slug': self.slug})

    def __unicode__(self):
        return self.name


def next_or_prev_in_order(instance, prev=False, qs=None, loop=False):
    """Get the next (or previous with prev=True) item for instance, from the
       given queryset (which is assumed to contain instance) respecting
       queryset ordering. If loop is True, return the first/last item when the
       end/start is reached. """

    if not qs:
        qs = instance.__class__.objects
    if prev:
        qs = qs.reverse()
        lookup = 'lt'
    else:
        lookup = 'gt'

    q_list = []
    prev_fields = []
    if qs.model._meta.ordering:
        ordering = list(qs.model._meta.ordering)
    else:
        ordering = []

    for field in (ordering + ['pk']):
        if field[0] == '-':
            this_lookup = (lookup == 'gt' and 'lt' or 'gt')
            field = field[1:]
        else:
            this_lookup = lookup
        q_kwargs = dict([(f, get_model_attr(instance, f))
                         for f in prev_fields])
        key = "%s__%s" % (field, this_lookup)
        q_kwargs[key] = get_model_attr(instance, field)
        q_list.append(models.Q(**q_kwargs))
        prev_fields.append(field)
    try:
        return qs.filter(reduce(models.Q.__or__, q_list))[0]
    except IndexError:
        length = qs.count()
        if loop and length > 1:
            # queryset is reversed above if prev
            return qs[0]
    return None


class New(models.Model):
    titulo = models.CharField('News name', max_length=10000)
    categoria = models.ForeignKey(CategoryNew, related_name='children')
    imagen_thumbnail = models.ImageField(upload_to="images", blank=True, null=True)
    #imagen_slide = models.ImageField(upload_to="images", blank=True, null=True)
    imagen1 = models.ImageField(upload_to="images", blank=True, null=True)
    sumario = models.TextField(max_length=10000, blank=True, null=True)
    texto1 = RichTextField(max_length=10000, blank=True, null=True)
    imagen2 = models.ImageField(upload_to="images", blank=True, null=True)
    texto2 = RichTextField(max_length=10000, blank=True, null=True)
    destacado1 = models.TextField(max_length=10000, blank=True, null=True)
    texto3 = RichTextField(max_length=10000, blank=True, null=True)
    imagen3 = models.ImageField(upload_to="images", blank=True, null=True)
    texto4 = RichTextField(max_length=10000, blank=True, null=True)
    destacado2 = models.TextField(max_length=10000, blank=True, null=True)
    texto5 = RichTextField(max_length=10000, blank=True, null=True)
    imagen4 = models.ImageField(upload_to="images", blank=True, null=True)
    texto6 = RichTextField(max_length=10000, blank=True, null=True)
    texto7 = RichTextField(max_length=10000, blank=True, null=True)
    imagen5 = models.ImageField(upload_to="images", blank=True, null=True)
    #etiquetas = models.ForeignKey(Tag, blank=True, null=True)
    tags = TagField()
    date = models.DateField('Published', blank=True, null=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    #slug = AutoSlugField(populate_from=titulo, unique_with='categoria_slug',blank=True, null=True, editable=True)
    metatag = models.ForeignKey(Metatag, blank=True, null=True)
    
    
    def get_tags(self):
        return Tag.objects.get_for_object(self)

    # def save(self, *args, **kwargs):
    #     self.slug = slugify(self.titulo)
    #     super(New, self).save(*args, **kwargs)

    def get_previous(self):
        """
        Returns the previous element of the list using the current index if it exists.
        Otherwise returns an empty string.
        """
        qs = self.__class__.objects.filter(categoria=self.categoria).order_by('pk')
        return next_or_prev_in_order(self, True, qs)
        # previous = self.__class__.objects.filter(pk__lt=self.pk)
        # try:
        #     return previous[0]
        # except IndexError:
        #     return False

    def get_next(self):
        """
        Get the next object by primary key order
        """
        qs = self.__class__.objects.filter(categoria=self.categoria).order_by('pk')
        return next_or_prev_in_order(self, False, qs)
        # next = self.__class__.objects.filter(pk__gt=self.pk)
        # try:
        #     return next[0]
        # except IndexError:
        #     return False
        
    def get_absolute_url(self):
        return reverse('news_detail', kwargs={'category':self.categoria.slug, 'slug': self.slug})

    def __unicode__(self):
        return self.titulo

class NewsLetterSuscription(models.Model):
    correo_electronico = models.TextField(max_length=100, blank=True, null=True)
    def __unicode__(self):
        return self.correo_electronico

class Contacto(models.Model):
    nombre_contacto = models.CharField(max_length=100)
    sexo = models.CharField(max_length=100,blank=True,null=True)
    edad = models.CharField(max_length=10,blank=True,null=True)
    ciudad = models.CharField(max_length=100,blank=True,null=True)
    estado = models.CharField(max_length=100,blank=True,null=True)
    empresa = models.CharField(max_length=100,blank=True,null=True)
    correo_electronico = models.CharField(max_length=150,blank=True,null=True)

    def __unicode__(self):
        return self.nombre_contacto

class Franquiciatario(models.Model):
    nombre_contacto = models.CharField(max_length=200,blank=True,null=True)
    correo_electronico = models.CharField(max_length=150,blank=True,null=True)
    telefono = models.CharField(max_length=100,blank=True,null=True)
    comentario = models.CharField(max_length=1500,blank=True,null=True)    

    def __unicode__(self):
        return self.nombre_contacto
